local SMELT_TIME = 10

local Fuel
local Input
local Output

local Arrow

local CurrentIndex = nil

local Furnaces = {}

function Vec3ToString(v)
    return v.x .. v.y .. v.z
end

function Init()
    local arrow = {
        0,  0, 5, 0,  5, 20,
        0,  0, 5, 20, 0, 20,
        -5, 0, 3, -8, 10, 0
    }

    Interface.Set_Document("furnace")

    Interface.Add_Background("bg", UI.Scale(200, 400), UI.Scale(180, 180), true, vec2(0, 0), UI.Scale(10, 10))

    Fuel   = Interface.Add_Slot("fuel",   UI.Scale(240, 475), UI.Scale(40, 40).y)
    Input  = Interface.Add_Slot("input",  UI.Scale(300, 520), UI.Scale(40, 40).y)
    Output = Interface.Add_Slot("output", UI.Scale(300, 430), UI.Scale(40, 40).y)

    Output.OutputOnly = true

    Arrow = Interface.Add_Custom("arrow", UI.Scale(320, 495), arrow)
    Arrow.Color = vec3(0, 0, 0)

    Interface.Set_Document("")
end

function Right_Click()
    UI.Set_Custom_Document("furnace")
    UI.Set_Mouse_Active(true)

    CurrentIndex = Vec3ToString(Player.Get_Looking_Pos())

    if Furnaces[CurrentIndex] == nil then
        Furnaces[CurrentIndex] = {
            Fuel      = Stack(0, 0, 0),
            Input     = Stack(0, 0, 0),
            Output    = Stack(0, 0, 0),
            Progress  = 0,
            Remaining = 0
        }
    end

    local f = Furnaces[CurrentIndex]

    Fuel:Set(Furnaces[CurrentIndex].Fuel)
    Input:Set(Furnaces[CurrentIndex].Input)
    Output:Set(Furnaces[CurrentIndex].Output)
end

function Update(DeltaTime)
    if CurrentIndex ~= nil then
        Furnaces[CurrentIndex].Fuel = Fuel.Contents
        Furnaces[CurrentIndex].Input = Input.Contents
        Furnaces[CurrentIndex].Output = Output.Contents
    end

    for _, f in pairs(Furnaces) do
        if f.Remaining > 0 then
            f.Remaining = f.Remaining - DeltaTime
        end

        if f.Remaining < 0 then
            f.Remaining = 0
        end

        if f.Input.Type == 0 then goto continue end

        do
            local inputType = Get_Block(f.Input)

            if not inputType.Smeltable then goto continue end

            if f.Output.Type > 0 and f.Output ~= inputType.SmeltResult then
                goto continue
            end

            local p = (f.Progress / SMELT_TIME) * 255
            if p > 255 then p = 255 end

            Arrow.Color = vec3(p, p, p)

            if f.Remaining > 0 then
                f.Progress = f.Progress + DeltaTime

                if f.Progress >= SMELT_TIME then
                    f.Progress = 0

                    if f.Output.Type > 0 then
                        f.Output.Size = f.Output.Size + 1
                    else
                        f.Output = inputType.SmeltResult
                    end

                    f.Input:Decrease()
                end
            end
        end

        do
            local fuelType = Get_Block(f.Fuel)

            if fuelType.BurnTime > 0 then
                if f.Remaining == 0 and f.Fuel.Type > 0 then
                    f.Fuel:Decrease()
                    f.Remaining = fuelType.BurnTime
                end
            end
        end

        ::continue::

        Fuel:Set(f.Fuel)
        Input:Set(f.Input)
        Output:Set(f.Output)
    end
end

function Close()
    CurrentIndex = nil;

    UI.Set_Custom_Document("")
    UI.Set_Mouse_Active(false)
end
