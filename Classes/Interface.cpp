#include "Interface.h"

#include <algorithm>

#include <glm/gtc/matrix_transform.hpp>

// Ignore warnings in FreeType library.

#ifndef _WIN32
	#pragma clang diagnostic push

	#pragma clang diagnostic ignored "-Wdocumentation-unknown-command"
	#pragma clang diagnostic ignored "-Wdocumentation"
	#pragma clang diagnostic ignored "-Wold-style-cast"
	#pragma clang diagnostic ignored "-Wreserved-id-macro"
#endif

#include <freetype2/ft2build.h>
#include FT_FREETYPE_H

#ifndef _WIN32
	#pragma clang diagnostic pop
#endif

#include <FreeImagePlus.h>
#include <unicode/ustream.h>

#include "UI.h"
#include "main.h"
#include "Blocks.h"
#include "Shader.h"
#include "System.h"
#include "Inventory.h"

#ifdef WIN32
    #undef min
    #undef max
#endif

#ifdef __APPLE__

static errno_t localtime_s(std::tm* tm, std::time_t* time) {
    tm = std::localtime(time);
    return tm == nullptr;
}

#endif

bool        Interface::Holding         = false;

void*       Interface::ActiveElement   = nullptr;
void*       Interface::HoveringElement = nullptr;

std::string Interface::ActiveType      = "";
std::string Interface::HoveringType    = "";
std::string Interface::ActiveDocument  = "";

std::map<
    std::string, std::map<std::string, std::shared_ptr<BaseElement>>
> Documents = {};

const std::string FONT      = "Roboto";
const int         FONT_SIZE = 15;

static Shader* TextShader;
static Shader* UI3DShader;
static Shader* UIBorderShader;
static Shader* UITextureShader;
static Shader* UIBackgroundShader;

static glm::ivec2 TEXT_ATLAS_SIZE = {0, 0};

static int       TEXT_BOX_HORZ_PADDING;

static float     BAR_PADDING;
static float     SLIDER_WIDTH;
static float     TEXT_PADDING;
static float     BUTTON_PADDING;
static float     SLIDER_PADDING;
static float     SLOT_BLOCK_SCALE;

static glm::vec2 SLOT_SIZE;
static glm::vec2 SLOT_PADDING;
static glm::vec2 SLOT_TEXT_PADDING;

const  int       TEXT_TEXTURE_UNIT         = 10;
const  int       TEXT_GLYPHS               = 1024;

const  glm::vec3 SLOT_BG_COLOR             = glm::vec3(0.1f);
const  glm::vec3 SLOT_HOVER_COLOR          = glm::vec3(0.7f);

const  float     BUTTON_OPACITY            = 1.0f;
const  float     BUTTON_TEXT_OPACITY       = 1.0f;
const  glm::vec3 BUTTON_COLOR              = glm::vec3(0.5f);
const  glm::vec3 BUTTON_HOVER_COLOR        = glm::vec3(0.7f);
const  glm::vec3 BUTTON_CLICK_COLOR        = glm::vec3(0.3f, 0.3f, 0.8f);
const  glm::vec3 BUTTON_TEXT_COLOR         = glm::vec3(1.0f);

const  float     SLIDER_OPACITY            = 1.0f;
const  float     SLIDER_HANDLE_OPACITY     = 1.0f;
const  float     SLIDER_TEXT_OPACITY       = 1.0f;
const  glm::vec3 SLIDER_COLOR              = glm::vec3(0.5f);
const  glm::vec3 SLIDER_HANDLE_COLOR       = glm::vec3(0.7f);
const  glm::vec3 SLIDER_HANDLE_HOVER_COLOR = glm::vec3(0.9f);
const  glm::vec3 SLIDER_HANDLE_CLICK_COLOR = glm::vec3(0.3f, 0.3f, 0.8f);
const  glm::vec3 SLIDER_TEXT_COLOR         = glm::vec3(1.0f);

const  float     BAR_OPACITY               = 1.0f;
const  float     BAR_TEXT_OPACITY          = 1.0f;
const  float     BAR_BACKGROUND_OPACITY    = 1.0f;
const  glm::vec3 BAR_BACKGROUND_COLOR      = glm::vec3(0.0f, 1.0f, 0.0f);
const  glm::vec3 BAR_COLOR                 = glm::vec3(0.2f);
const  glm::vec3 BAR_TEXT_COLOR            = glm::vec3(1.0f);

const  float     BACKGROUND_OPACITY        = 0.7f;
const  glm::vec3 BACKGROUND_COLOR          = glm::vec3(0.0f);
const  glm::vec3 BACKGROUND_BORDER_COLOR   = glm::vec3(0.5f);

const std::map<char, glm::vec3> ColorCodes = {
    {'0', {0.000, 0.000, 0.000} }, // Black
    {'1', {0.000, 0.000, 0.666} }, // Dark Blue
    {'2', {0.000, 0.666, 0.000} }, // Dark Green
    {'3', {0.000, 0.666, 0.666} }, // Dark Aqua
    {'4', {0.666, 0.000, 0.000} }, // Dark Red
    {'5', {0.666, 0.000, 0.666} }, // Dark Purple
    {'6', {1.000, 0.666, 0.000} }, // Gold
    {'7', {0.666, 0.666, 0.666} }, // Gray
    {'8', {0.333, 0.333, 0.333} }, // Dark Gray
    {'9', {0.333, 0.333, 1.000} }, // Blue
    {'a', {0.333, 1.000, 0.333} }, // Green
    {'b', {0.333, 1.000, 1.000} }, // Aqua
    {'c', {1.000, 0.333, 0.333} }, // Red
    {'d', {1.000, 0.333, 1.000} }, // Light Purple
    {'e', {1.000, 1.000, 0.333} }, // Yellow
    {'f', {1.000, 1.000, 1.000} }, // White
};

struct CharacterInfo {
  glm::vec2 Advance;
  glm::vec2 BitmapSize;
  glm::vec2 BitmapOffset;

  glm::vec2 Offset = {0, 0};
};

static std::map<int, CharacterInfo> Characters;

Data Get_3D_Mesh(const Block* block, float x, float y, bool offsets) {
    Data data;

    // Magic number that seems to work :P
    x *= 2.005f;
    y *= 2.005f;

    if (block->HasIcon) {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                Extend(data, vertices[i][j]);
                Extend(data, tex_coords[i][j]);
                data.push_back(static_cast<float>(block->Icon));

                if (offsets) {
                    Extend(data, x, y);
                }
            }
        }
    }

    else if (block->HasCustomData) {
        for (auto const &element : block->CustomData) {
            for (unsigned long i = 0; i < 6; i++) {
                for (unsigned long j = 0; j < 6; j++) {
                    Extend(data, element[i][j].first);
                    Extend(data, element[i][j].second);

                    if (offsets) {
                        Extend(data, x, y);
                    }
                }
            }
        }
    }

    else {
        for (unsigned long i = 0; i < 6; i++) {
            for (unsigned long j = 0; j < 6; j++) {
                Extend(data, vertices[i][j]);

                if (block->MultiTextures) {
                    Extend(data, tex_coords[i][j]);
                    data.push_back(static_cast<float>(block->Textures[i]));
                }
                else if (block->HasTexture) {
                    Extend(data, tex_coords[i][j]);
                    data.push_back(static_cast<float>(block->Texture));
                }

                if (offsets) {
                    Extend(data, x, y);
                }
            }
        }
    }

    return data;
}

std::tuple<unsigned int, int, int> Load_Texture(std::string file, bool mipmap, float afLevel) {
    std::string path = "Images/" + file;

    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mipmap ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, afLevel);

    fipImage image;
    image.load(path.c_str());
    image.flipVertical();

    int width  = static_cast<int>(image.getWidth());
    int height = static_cast<int>(image.getHeight());
    unsigned char* imageData = image.accessPixels();

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, imageData);
    glBindTexture(GL_TEXTURE_2D, 0);

    return std::make_tuple(texture, width, height);
}

unsigned int Load_Array_Texture(std::string file, glm::ivec2 subCount, int mipmap, float afLevel) {
    std::string path = "Images/" + file;

    unsigned int texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D_ARRAY, texture);

    fipImage image;
    image.load(path.c_str());
    image.flipVertical();

    int width  = static_cast<int>(image.getWidth());
    int height = static_cast<int>(image.getHeight());

    glm::ivec2 subSize(width / subCount.x, height / subCount.y);

    glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAX_ANISOTROPY_EXT, afLevel);

    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,     GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,     GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    if (mipmap > 0) {
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
    }
    else {
        glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }

    glTexStorage3D(
        GL_TEXTURE_2D_ARRAY, mipmap + 1, GL_RGBA8,
        subSize.x, subSize.y, (width * height) / (subSize.x * subSize.y)
    );

    int layer = 0;

    for (int h = 0; h < height; h += subSize.y) {
        for (int w = 0; w < width; w += subSize.x) {
            fipImage subImg;
            image.copySubImage(subImg, w, height - h, w + subSize.x, height - h - subSize.y);

            glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0,
                layer++, subSize.x, subSize.y, 1, GL_BGRA, GL_UNSIGNED_BYTE,
                reinterpret_cast<void*>(subImg.accessPixels())
            );
        }
    }

    if (mipmap > 0) {
        glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
    }

    glBindTexture(GL_TEXTURE_2D_ARRAY, 0);

    return texture;
}

void Take_Screenshot() {
    // Get the current time and set it as the filename.
    std::tm tm;
    std::time_t t = std::time(nullptr);
    localtime_s(&tm, &t);
    std::stringstream ss;

    ss << std::put_time(&tm, "%F %H.%M.%S");
    std::string fileName = ss.str() + ".bmp";

    // Store the OpenGL pixel data in a buffer.
    unsigned char* pixelData = static_cast<unsigned char*>(
        malloc(static_cast<unsigned long>(4 * SCREEN_WIDTH * SCREEN_HEIGHT))
    );
    glReadPixels(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, GL_BGRA, GL_UNSIGNED_BYTE, pixelData);

    // Convert the raw data into an image.
    fipImage img;
    img = FreeImage_ConvertFromRawBits(
        pixelData, SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_WIDTH * 4, 32,
        FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, false
    );

    // Save the image.
    img.save(fileName.c_str());

    // Free the buffer memory.
    free(static_cast<void*>(pixelData));
}

#ifndef _WIN32
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

bool BaseElement::Get_Hover(int cursorX, int cursorY, bool holding) { return false; }
void BaseElement::Draw() {}

#ifndef _WIN32
#pragma clang diagnostic pop
#endif

Custom::Custom(std::string name, float x, float y, Data &data) {
    X = x;
    Y = y;
    Name = name;

    ModelMatrix = glm::translate(ModelMatrix, glm::vec3(X, Y, 0));

    Storage.Init(UIBackgroundShader);
    Storage.Create(2, data);
}

void Custom::Draw() {
    UIBackgroundShader->Upload("color", Color);
    UIBackgroundShader->Upload("model", ModelMatrix);
    Storage.Draw();
    UIBackgroundShader->Upload("model", glm::mat4());
}

void TextElement::Init(std::string name, std::string text, float x, float y, float opacity, glm::vec3 color, float scale) {
	x = std::floor(x);
	y = std::floor(y);

    OriginalX = x;
    OriginalY = y;

    X = x;
    Y = y;
    Name = name;
    Text = text;
    Color = color;
    Scale = scale;
    Opacity = opacity;
    Width = Get_Width();

    TextBuffer.Init(TextShader);
    TextBuffer.Create(2, 2, 3);

    Mesh();
}

void TextElement::Center(float x, float y, float width, glm::bvec2 axes) {
    Centered = axes;
    CenterWidth = width;

    if (axes.x) {
        X = std::floor(x + (width - Width) / 2);
    }

    if (axes.y) {
        Y = std::floor(y + TEXT_PADDING - float(FONT_SIZE / 6));
    }
}

void TextElement::Set_Text(std::string newText) {
    Text = newText;
    Width = Get_Width();

    Center(OriginalX, OriginalY, CenterWidth, Centered);
    Mesh();
}

void TextElement::Mesh() {
    Data data;

    bool skipNext = false;
    glm::vec2 charPos = {0, 0};
    glm::vec3 textColor = Color;

    for (char const &c : Text) {
        if (c == '&' || skipNext) {
            if (skipNext && c != '&') {
                try {
                    textColor = ColorCodes.at(c);
                }
                catch (const std::out_of_range) {
                    throw std::runtime_error("Error! Invalid color code in string \"" + Text + "\".");
                }
            }

            skipNext = (c == '&');
            continue;
        }

        CharacterInfo ch = Characters[c];

        glm::vec2 size = ch.BitmapSize * Scale;
        glm::vec2 p1 = charPos + ch.BitmapOffset * Scale - glm::vec2(0, size.y);
        glm::vec2 p2 = p1 + size;

        charPos += ch.Advance * Scale;

        if (size.x == 0 || size.y == 0) {
            continue;
        }

        glm::vec2 t1 = ch.Offset;
        glm::vec2 t2 = t1 + ch.BitmapSize / static_cast<glm::vec2>(TEXT_ATLAS_SIZE);

        Extend(data,
            p1.x, p1.y, t1.x, t2.y, EXPAND_VEC3(textColor),
            p2.x, p1.y, t2.x, t2.y, EXPAND_VEC3(textColor),
            p2.x, p2.y, t2.x, t1.y, EXPAND_VEC3(textColor),
            p1.x, p1.y, t1.x, t2.y, EXPAND_VEC3(textColor),
            p2.x, p2.y, t2.x, t1.y, EXPAND_VEC3(textColor),
            p1.x, p2.y, t1.x, t1.y, EXPAND_VEC3(textColor)
        );
    }

    TextBuffer.Upload(data);
}

void TextElement::Draw() {
    if (Opacity == 0.0f || Text == "") {
        return;
    }

    TextShader->Upload("Position", glm::vec2(X, Y));
    TextShader->Upload("Opacity", Opacity);
    TextBuffer.Draw();
}

float TextElement::Get_Width() {
    float width = 0;
    bool skipNext = false;

    for (char const &c : Text) {
        if (c == '&' || skipNext) {
            skipNext = (c == '&');
            continue;
        }

        width += Characters[c].Advance.x * Scale;
    }

    return width;
}

void Button::Init(std::string name, std::string text, glm::vec4 dims, Func function) {
    Name = name;
    Function = function;

    X = dims.x;
    Y = dims.y;
    Width = dims.z;
    Height = (dims.w != 0) ? dims.w : BUTTON_PADDING * 2;

    Opacity = BUTTON_OPACITY;
    Color = BUTTON_COLOR;

    Text.Init(name, text, X, Y);
    Text.Center(X, Y, Width);

    Text.Opacity = BUTTON_TEXT_OPACITY;
    Text.Color = BUTTON_TEXT_COLOR;

    BackgroundBuffer.Init(UIBackgroundShader);
    BorderBuffer.Init(UIBorderShader);

    BackgroundBuffer.Create(2, Data {
        X, Y + Height, X, Y, X + Width, Y, X, Y + Height, X + Width, Y, X + Width, Y + Height
    });
    BorderBuffer.Create(2, Data {
        X, Y, X + Width, Y, X + Width, Y + Height, X, Y + Height
    });

    BorderBuffer.VertexType = GL_LINE_LOOP;
}

void Button::Release() {
    Color = BUTTON_COLOR;
    Function(this);

    Interface::HoveringType = "";
    Interface::HoveringElement = nullptr;
}

bool Button::Get_Hover(int cursorX, int cursorY, bool holding) {
    if (In_Range(cursorX, X, Width)) {
        if (In_Range(cursorY, Y, Height)) {
            if (holding) {
                Color = BUTTON_CLICK_COLOR;
            }
            else {
                Color = BUTTON_HOVER_COLOR;
            }

            Interface::HoveringType    = "button";
            Interface::HoveringElement = this;

            System::Set_Cursor(GLFW_HAND_CURSOR);
            return true;
        }
    }

    return false;
}

void Button::Draw() {
    UIBackgroundShader->Upload("color", Color);
    UIBackgroundShader->Upload("alpha", Opacity);

    BackgroundBuffer.Draw();

    UIBorderShader->Upload("color", glm::vec3(0));

    BorderBuffer.Draw();
    Text.Draw();
}

void Slider::Init(std::string name, std::string text, glm::vec4 dims, glm::vec3 range, Func function) {
    Name = name;
    Function = function;

    X = dims.x;
    Y = dims.y;
    Width = dims.z;
    Height = (dims.w == 0) ? SLIDER_PADDING * 2 : dims.w;

    Min = range.x;
    Max = range.y;
    Value = range.z;

    Color = SLIDER_COLOR;
    Opacity = SLIDER_OPACITY;
    HandleColor = SLIDER_HANDLE_COLOR;
    HandleOpacity = SLIDER_HANDLE_OPACITY;

    Text.Init(name, text, X, Y);
    Text.Center(X, Y, Width);

    Text.Opacity = SLIDER_TEXT_OPACITY;
    Text.Color = SLIDER_TEXT_COLOR;

    float sw = SLIDER_WIDTH / 2;
    float sx = X + Width * ((Value - Min) / (Max - Min));
    HandlePosition = sx;

    BackgroundBuffer.Init(UIBackgroundShader);
    HandleBuffer.Init(UIBackgroundShader);
    BorderBuffer.Init(UIBorderShader);

    BackgroundBuffer.Create(2, Data {
        X, Y + Height, X, Y, X + Width, Y, X, Y + Height, X + Width, Y, X + Width, Y + Height
    });
    BorderBuffer.Create(2, Data {
        X, Y, X + Width, Y, X + Width, Y + Height, X - 0.5f, Y + Height
    });
    HandleBuffer.Create(2, Data {
        sx, Y + Height, sx, Y, sx + sw, Y, sx, Y + Height, sx + sw, Y, sx + sw, Y + Height
    });

    BorderBuffer.VertexType = GL_LINE_LOOP;
}

void Slider::Release() {
    HandleColor = SLIDER_HANDLE_COLOR;
    Move(std::ceil(Value), true);
    Function(this);
}

bool Slider::Get_Hover(int cursorX, int cursorY, bool holding) {
    if (In_Range(cursorX, HandlePosition, SLIDER_WIDTH)) {
        if (In_Range(cursorY, Y, Height)) {
            if (holding) {
                HandleColor = SLIDER_HANDLE_CLICK_COLOR;
            }
            else {
                HandleColor = SLIDER_HANDLE_HOVER_COLOR;
            }

            Interface::HoveringType    = "slider";
            Interface::HoveringElement = this;

            System::Set_Cursor(GLFW_HAND_CURSOR);
            return true;
        }
    }

    return false;
}

void Slider::Move(float position, bool setValue) {
    float w = SLIDER_WIDTH / 2;
    float x;

    if (setValue) {
        x = X + Width * ((position - 0.5f) / (Max - Min)) - w;
    }
    else {
        if (position <= X + w) {
            position = X + w;
        }
        else if (position >= X + Width - 1) {
            position = X + Width - 1;
        }

        float percentage = (position - X) / Width;
        x = X + Width * percentage - w;

        int oldValue = static_cast<int>(std::round(Value));
        Value = (Max - Min) * percentage;

        Text.Text.replace(
            Text.Text.find(std::to_string(oldValue)),
            std::to_string(oldValue).length(),
            std::to_string(static_cast<int>(std::round(Value)))
        );
        Text.Mesh();
    }

    HandleBuffer.Upload(Data {x, Y + Height, x, Y, x + w, Y,  x, Y + Height, x + w, Y, x + w, Y + Height});
    HandlePosition = x;
}

void Slider::Draw() {
    UIBackgroundShader->Upload("color", Color);
    UIBackgroundShader->Upload("alpha", Opacity);

    BackgroundBuffer.Draw();

    UIBorderShader->Upload("color", glm::vec3(0));
    BorderBuffer.Draw();

    Text.Draw();

    UIBackgroundShader->Upload("color", HandleColor);
    UIBackgroundShader->Upload("alpha", HandleOpacity);

    HandleBuffer.Draw();
}

void Bar::Init(std::string name, std::string text, glm::vec4 dims, glm::vec3 range) {
    Name = name;

    X = dims.x;
    Y = dims.y;
    Width = dims.z;
    Height = (dims.w == 0) ? BAR_PADDING * 2 : dims.w;

    Min = range.x;
    Max = range.y;
    Value = range.z;

    Color = BAR_BACKGROUND_COLOR;
    Opacity = BAR_BACKGROUND_OPACITY;

    BarColor = BAR_COLOR;
    BarOpacity = BAR_OPACITY;

    Text.Init(name, text, X, Y);
    Text.Opacity = BAR_TEXT_OPACITY;
    Text.Color = BAR_TEXT_COLOR;

    BackgroundBuffer.Init(UIBackgroundShader);
    BarBuffer.Init(UIBackgroundShader);
    BorderBuffer.Init(UIBorderShader);

    BackgroundBuffer.Create(2, Get_Rect(X, X + Width, Y, Y + Height));
    BarBuffer.Create(2);
    BorderBuffer.Create(2, Get_Border(X, X + Width, Y, Y + Height));

    BorderBuffer.VertexType = GL_LINE_LOOP;
}

void Bar::Move(float value) {
    Value = value;

    float barEndX = X + Width * ((Value - Min) / (Max - Min));
    BarBuffer.Upload(Get_Rect(X, barEndX, Y, Y + Height));
}

void Bar::Draw() {
    UIBackgroundShader->Upload("color", Color);
    UIBackgroundShader->Upload("alpha", Opacity);

    BackgroundBuffer.Draw();

    UIBorderShader->Upload("color", glm::vec3(0));
    BorderBuffer.Draw();

    Text.Draw();

    UIBackgroundShader->Upload("color", BarColor);
    UIBackgroundShader->Upload("alpha", BarOpacity);

    BarBuffer.Draw();
}

void Image::Init(std::string name, std::string file, int texID, glm::vec3 dims) {
    Name = name;
    TexID = texID;

    X = dims.x;
    Y = dims.y;
    Scale = dims.z;

    glActiveTexture(GL_TEXTURE0 + static_cast<unsigned int>(TexID));

	auto imageData = Load_Texture(file);
	Texture = std::get<0>(imageData);
	Width = static_cast<float>(std::get<1>(imageData)) * Scale;
	Height = static_cast<float>(std::get<2>(imageData)) * Scale;

    glBindTexture(GL_TEXTURE_2D, Texture);

    ImageBuffer.Init(UITextureShader);
    ImageBuffer.Create(2, 2, Get_Tex_Rect(X, X + Width, Y, Y + Height));
}

void Image::Center() {
    X = (SCREEN_WIDTH - Width) / 2;
    ImageBuffer.Upload(Get_Tex_Rect(X, X + Width, Y, Y + Height));
}

void Image::Draw() {
    UITextureShader->Upload("tex", TexID);
    ImageBuffer.Draw();
}

void Background::Init(std::string name, glm::vec4 dims, bool border, glm::vec2 gridWidth, glm::vec2 pad) {
    Name = name;

    X = dims.x;
    Y = dims.y;
    Width = dims.z;
    Height = dims.w;

    Color = BACKGROUND_COLOR;
    Opacity = BACKGROUND_OPACITY;
    GridColor = BACKGROUND_BORDER_COLOR;

    BackgroundBuffer.Init(UIBackgroundShader);
    BackgroundBuffer.Create(2, Get_Rect(X, X + Width, Y, Y + Height));

    if (border) {
        GridSet = true;
        GridBuffer.Init(UIBorderShader);

        Data gridData;

        if (gridWidth != glm::vec2(0, 0)) {
            GridWidth = gridWidth;

            for (float gx = X + pad.x; gx <= X + Width - pad.x; gx += GridWidth.x) {
                Extend(gridData, gx, Y + pad.y, gx, Y + Height - pad.y);
            }

            for (float gy = Y + pad.y; gy <= Y + Height - pad.y; gy += GridWidth.y) {
                Extend(gridData, X + pad.x, gy, X + Width - pad.x, gy);
            }

            // Fix for missing pixel in upper-left corner.
            Extend(gridData, X + pad.x - 0.5f, Y + Height - pad.y, X + pad.x, Y + Height - pad.y);
        }
        else {
            Extend(gridData, Get_Border(X + 5, X + Width - 5, Y + 5, Y + Height - 5));
        }

        GridBuffer.Create(2, gridData);
        GridBuffer.VertexType = GL_LINES;
    }
}

void Background::Move(float dx, float dy, bool absolute) {
    if (absolute) {
        X = dx;
		Y = dy;
    }
    else {
        X += dx;
        Y += dy;
    }

    BackgroundBuffer.Upload(Get_Rect(X, X + Width, Y, Y + Height));

    if (GridWidth != glm::vec2(0, 0)) {
        Data gridData;

        for (float gx = X; gx <= X + Width; gx += GridWidth.x) {
            Extend(gridData, gx, Y, gx, Y + Height);
        }

        for (float gy = Y; gy <= Y + Height; gy += GridWidth.y) {
            Extend(gridData, X, gy, X + Width, gy);
        }

        Extend(gridData, X - 0.5f, Y + Height, X + 0.5f, Y + Height);

        GridBuffer.Upload(gridData);
    }
    else if (GridSet) {
        GridBuffer.Upload(Get_Border(X, X + Width, Y, Y + Height));
    }
}

void Background::Draw() {
    UIBackgroundShader->Upload("color", Color);
    UIBackgroundShader->Upload("alpha", Opacity);
    BackgroundBuffer.Draw();

    if (GridSet) {
        UIBorderShader->Upload("color", GridColor);
        GridBuffer.Draw();
    }
}

OrthoElement::OrthoElement(std::string name, int type, int data, float x, float y, float scale) {
    OrthoBuffer.Init(UI3DShader);

    X = x;
    Y = y;
    Name = name;
    Type = type;
    Scale = scale;

    if (type == 0) {
        OrthoBuffer.Create(3, 3, 2);
    }
    else {
        OrthoBuffer.Create(3, 3, 2, Get_3D_Mesh(Blocks::Get_Block(Type, data), X, Y, true));
    }
}

void OrthoElement::Mesh(int type, int data, float x, float y) {
    if (x != 0) {
        X = x;
    }

    if (y != 0) {
        Y = y;
    }

    Type = type;

    if (Type == 0) {
        return;
    }

    const Block* block = Blocks::Get_Block(type, data);
    ModelMatrix = glm::mat4();

    if (!block->HasIcon) {
        ModelMatrix = glm::rotate(ModelMatrix, glm::radians(20.0f), glm::vec3(1, 0, 0));
        ModelMatrix = glm::rotate(ModelMatrix, 45.0f, glm::vec3(0, 1, 0));
    }

    OrthoBuffer.Upload(Get_3D_Mesh(block, X, Y, true));
}

void OrthoElement::Draw() {
    if (Type == 0) {
        return;
    }

    UI3DShader->Upload("model", ModelMatrix);
    UI3DShader->Upload("scale", Scale);
    OrthoBuffer.Draw();
}

void TextBox::Init(std::string name, glm::vec4 dims) {
    Name = name;

    X = dims.x;
    Y = dims.y;
    Width = dims.z;
    Height = dims.w;

    int textPad = static_cast<int>((Height - FONT_SIZE) / 2);

    BG = Background(name, X, Y, Width, Height, true);

    Highlight = Background(name, X, Y, 0, Height - textPad * 2, false);
    Highlight.Color = glm::vec3(1);
    Highlight.Opacity = 0.5f;

    MaxWidth = Width - TEXT_BOX_HORZ_PADDING * 2;

    Cursor.Init(name, "|", X + TEXT_BOX_HORZ_PADDING, Y + textPad);
    TextEl.Init(name, Text, X + TEXT_BOX_HORZ_PADDING, Y + textPad);

    Cursor.Opacity = 0;
}

void TextBox::Key_Handler(int key) {
    if (KeyModifers & CTRL_FLAG) {
        if (key == GLFW_KEY_C) {
			glfwSetClipboardString(Window, MarkedText.c_str());
        }
        else if (key == GLFW_KEY_V) {
            Paste();
        }
        else if (key == GLFW_KEY_X) {
			glfwSetClipboardString(Window, MarkedText.c_str());
            Text.replace(SelectStart, MarkedText.length(), "");
            MarkedText = "";

            CursorPos = SelectStart;
            MarkerPos = CursorPos;

            Update();
        }
    }

    if (key == GLFW_KEY_BACKSPACE && CursorPos > 0) {
        if (MarkedText != "") {
            Text.replace(SelectStart, MarkedText.length(), "");
            MarkedText = "";

            CursorPos = SelectStart;
            MarkerPos = CursorPos;
        }
        else {
            --CursorPos;
            Text.erase(CursorPos, 1);
        }

        Update();
    }

    else if (key == GLFW_KEY_LEFT && CursorPos > 0) {
        if (KeyModifers & GLFW_MOD_SHIFT && MarkerPos > 0) {
            --MarkerPos;
            Select(CursorPos, MarkerPos);
        }
        else {
            --CursorPos;
            MarkedText = "";
            MarkerPos = CursorPos;
            Update();
        }
    }

    else if (key == GLFW_KEY_RIGHT && CursorPos < Text.length()) {
        if (KeyModifers & GLFW_MOD_SHIFT && MarkerPos < Text.length()) {
            ++MarkerPos;
            Select(CursorPos, MarkerPos);
        }
        else {
            ++CursorPos;
            MarkedText = "";
            MarkerPos = CursorPos;
            Update();
        }
    }
}

void TextBox::Input(unsigned int codepoint) {
    UnicodeString string(static_cast<int32_t>(codepoint));
    std::string str;
    string.toUTF8String(str);

    // Ignore color characters
    if (str.front() == '&') {
        return;
    }

    Text.insert(CursorPos, str);
    ++CursorPos;

    if (MarkedText == "") {
        MarkerPos = CursorPos;
    }

    Update();
}

void TextBox::Paste() {
    std::string text = glfwGetClipboardString(Window);
    float maxLength  = MaxWidth;

    if (MarkedText == "") {
        maxLength -= TextEl.Width;
        std::string partString = Interface::Get_Fitting_String(
            text, static_cast<int>(maxLength)
        )[0];

        Text.insert(CursorPos, partString);
        CursorPos += static_cast<unsigned long>(partString.length());
    }
    else {
        Text.replace(SelectStart, MarkedText.length(), text);
        Select(SelectStart, SelectStart + static_cast<unsigned long>(text.length()));
    }

    Update();
}

void TextBox::Add_Text(std::string text) {
    float newTextWidth = Interface::Get_String_Width(text);

    if (TextWidth + newTextWidth > MaxWidth) {
        return;
    }

    Text += text;
    TextWidth += newTextWidth;
    CursorPos += static_cast<unsigned long>(text.length());

    Update();
}

void TextBox::Set_Cursor_Visibility(bool cursorVisible) {
    Cursor.Opacity = cursorVisible;
}

void TextBox::Update() {
    TextWidth = Interface::Get_String_Width(Text);

    if (TextWidth > MaxWidth) {
        --CursorPos;
        Text.erase(CursorPos, 1);
        return;
    }

    Cursor.X = X + TEXT_BOX_HORZ_PADDING + Interface::Get_String_Width(Text.substr(0, CursorPos));

    TextEl.Set_Text(Text);
    TextEl.Mesh();
}

void TextBox::Drag() {
    Select(CursorPos, Get_Pos(UI::MouseX));
}

void TextBox::Select(unsigned long cursor, unsigned long mark) {
    MarkerPos   = mark;
    SelectEnd   = std::max(cursor, mark);
    SelectStart = std::min(cursor, mark);

    MarkedText  = Text.substr(SelectStart, (SelectEnd - SelectStart));

    float highlightX = X + TEXT_BOX_HORZ_PADDING + Interface::Get_String_Width(Text.substr(0, SelectStart));
    float highlightY = Y + static_cast<int>((Height - FONT_SIZE) / 2);

    Highlight.Width = Interface::Get_String_Width(MarkedText);
    Highlight.Move(highlightX, highlightY, true);
}

void TextBox::Clear() {
    Text       = "";
    MarkedText = "";

    CursorPos = 0;
    MarkerPos = 0;
    TextWidth = 0;

    Cursor.X = X + TEXT_BOX_HORZ_PADDING;

    TextEl.Set_Text("");
    TextEl.Mesh();
}

unsigned long TextBox::Get_Pos(int x) {
    int xPos = (x - static_cast<int>(X)) - TEXT_BOX_HORZ_PADDING;

    if (xPos >= TextEl.Width) { return static_cast<unsigned long>(Text.length()); }
    if (xPos <= 0)            { return 0; }

    auto strings      = Interface::Get_Fitting_String(Text, xPos);
    unsigned long pos = static_cast<unsigned long>(strings[0].length());

    float stringLength  = Interface::Get_String_Width(Text.substr(0, pos));
    float nextCharWidth = Interface::Get_String_Width(Text.substr(pos, 1));

    if ((xPos - stringLength) >= (nextCharWidth / 2)) {
        return (pos + 1);
    }

    return pos;
}

void TextBox::Click() {
    unsigned long pos = Get_Pos(UI::MouseX);

    CursorPos = pos;
    MarkerPos = pos;
    MarkedText = "";

    Cursor.X = X + TEXT_BOX_HORZ_PADDING + Interface::Get_String_Width(Text.substr(0, pos));
    Cursor.Opacity = 1;
}

void TextBox::Focus() {
    Focused = true;
    Cursor.Opacity = 1;

    Interface::ActiveType = "textBox";
    Interface::ActiveElement = this;
}

void TextBox::Unfocus() {
    Focused = false;
    Cursor.Opacity = 0;

    Interface::ActiveType = "";
    Interface::ActiveElement = nullptr;
}

#ifndef _WIN32
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

bool TextBox::Get_Hover(int cursorX, int cursorY, bool holding) {
    if (In_Range(cursorX, X, Width)) {
        if (In_Range(cursorY, Y, Height)) {
            Interface::HoveringType = "textBox";
            Interface::HoveringElement = this;

            System::Set_Cursor(GLFW_IBEAM_CURSOR);
            return true;
        }
    }

    return false;
}

#ifndef _WIN32
#pragma clang diagnostic pop
#endif

void TextBox::Draw() {
    if (Visible) {
        if (Focused) {
            LastBlink += DeltaTime;

            if (LastBlink >= CURSOR_BLINK_SPEED) {
                Cursor.Opacity = !(Cursor.Opacity == 1.0f);
                LastBlink = 0.0;
            }
        }

        BG.Draw();
        Cursor.Draw();
        TextEl.Draw();

        if (MarkedText != "") {
            Highlight.Draw();
        }
    }
}

void Slot::Init(std::string name, glm::vec2 pos, float scale, Stack contents) {
    X = pos.x;
    Y = pos.y;
    SlotSize = scale;

    Name = name;
    Contents = contents;

    Width  = Scale_X(SlotSize);
    Height = Scale_Y(SlotSize);

    glm::vec2 textPos  = pos + SLOT_TEXT_PADDING * (SlotSize / 80);
    glm::vec2 modelPos = pos + SLOT_PADDING      * (SlotSize / 80);

    BG = Background(Name, X, Y, Width, Height, true, Scale(scale));
    BG.Color = SLOT_BG_COLOR;
    ItemCount.Init(Name, std::to_string(Contents.Size), textPos.x, textPos.y, static_cast<float>(Contents.Size > 0));
    ItemModel = OrthoElement(Name, Contents.Type, Contents.Data, modelPos.x, modelPos.y, Width);

    Mesh();
}

void Slot::Set_Contents(const Stack &stack) {
    Contents = stack;
    Mesh();
}

void Slot::Hover() {
    Hovering = true;
    BG.Color = SLOT_HOVER_COLOR;
}

void Slot::Stop_Hover() {
    Hovering = false;
    BG.Color = SLOT_BG_COLOR;
}

void Slot::Mesh() {
    ItemCount.Opacity = (Contents.Size > 0 && Contents.Type > 0);
    ItemCount.Set_Text(std::to_string(Contents.Size));
    ItemModel.Mesh(Contents.Type, Contents.Data, glm::vec2(X, Y) + SLOT_PADDING * (SlotSize / 80));

    if (SyncedSlot != nullptr) {
        SyncedSlot->Set_Contents(Contents);
    }
}

bool Slot::Get_Hover(int cursorX, int cursorY, bool holding) {
    if (In_Range(cursorX, X, Width)) {
        if (In_Range(cursorY, Y, Height)) {
            Hovering = true;
            BG.Color = SLOT_HOVER_COLOR;

            if (holding) {
                Inventory::Dragging_Slot(this);
            }

            Interface::HoveringType = "slot";
            Interface::HoveringElement = this;
            return true;
        }
    }

    return false;
}

void Slot::Draw() {
    if (Hovering) {
        ItemModel.Draw();
        ItemCount.Draw();
        BG.Draw();
    }
    else {
        BG.Draw();
        ItemModel.Draw();
        ItemCount.Draw();
    }
}

void Interface::Init() {
    Init_UI_Scale();
    Init_Shaders();
    Init_Text();
}

void Interface::Init_UI_Scale() {
    TEXT_BOX_HORZ_PADDING = static_cast<int>(Scale_X(10));
    SLOT_BLOCK_SCALE      = std::min(Scale_X(80), Scale_Y(80));

    SLOT_PADDING          = Scale(10, 20);
    SLOT_TEXT_PADDING     = Scale(5, 10);

    SLIDER_WIDTH          = Scale_X(10);

    BUTTON_PADDING        = Scale_Y(20);
    SLIDER_PADDING        = Scale_Y(20);
    TEXT_PADDING          = Scale_Y(20);
    BAR_PADDING           = Scale_Y(20);

    SLOT_SIZE             = glm::vec2(SLOT_BLOCK_SCALE);
}

void Interface::Init_Shaders() {
    glActiveTexture(GL_TEXTURE0);

    UIBackgroundShader = new Shader("ui");
    UIBorderShader = new Shader("uiBorder");
    UITextureShader = new Shader("uiTex");
    UI3DShader = new Shader("ortho");

    glm::mat4 model;
    model = glm::rotate(model, glm::radians(20.0f), glm::vec3(1, 0, 0));
    model = glm::rotate(model, 45.0f, glm::vec3(0, 1, 0));

    glm::mat4 projection = glm::ortho(
        0.0f, static_cast<float>(SCREEN_WIDTH), 0.0f, static_cast<float>(SCREEN_HEIGHT)
    );

    UIBackgroundShader->Upload("projection", projection);
    UIBackgroundShader->Upload("model", glm::mat4());

    UIBorderShader->Upload("projection", projection);
    UIBorderShader->Upload("color", glm::vec3(0));

    UITextureShader->Upload("projection", projection);
    UITextureShader->Upload("tex", 0);

    UI3DShader->Upload("tex", 0);
    UI3DShader->Upload("model", model);
    UI3DShader->Upload("projection", glm::ortho(
        0.0f, static_cast<float>(SCREEN_WIDTH), 0.0f,
        static_cast<float>(SCREEN_HEIGHT), -1000.0f, 1000.0f
    ));
}

void Interface::Init_Text() {
    TextShader = new Shader("text");

    FT_Library ft;
    FT_Face face;

    FT_Init_FreeType(&ft);
    FT_New_Face(ft, std::string("Fonts/" + FONT + ".ttf").c_str(), 0, &face);
    FT_Set_Char_Size(
        face, 0, FONT_SIZE * 34,
        static_cast<unsigned int>(SCREEN_DPI),
        static_cast<unsigned int>(SCREEN_DPI)
    );

    FT_GlyphSlot g = face->glyph;

    for (unsigned int c = 32; c < TEXT_GLYPHS; c++) {
        FT_Load_Char(face, c, FT_LOAD_RENDER);
        TEXT_ATLAS_SIZE.x += g->bitmap.width;
        TEXT_ATLAS_SIZE.y = std::max(TEXT_ATLAS_SIZE.y, static_cast<int>(g->bitmap.rows));
    }

    unsigned int textAtlas;
    glActiveTexture(GL_TEXTURE0 + TEXT_TEXTURE_UNIT);
    glGenTextures(1, &textAtlas);
    glBindTexture(GL_TEXTURE_2D, textAtlas);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(
        GL_TEXTURE_2D, 0, GL_RED,
        TEXT_ATLAS_SIZE.x, TEXT_ATLAS_SIZE.y,
        0, GL_RED, GL_UNSIGNED_BYTE, NULL
    );

    for (unsigned int c = 32; c < TEXT_GLYPHS; ++c) {
        static int xOffset = 0;

        CharacterInfo ch;

        if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
            continue;
        }

        glTexSubImage2D(
            GL_TEXTURE_2D, 0, xOffset, 0,
            static_cast<int>(g->bitmap.width), static_cast<int>(g->bitmap.rows),
            GL_RED, GL_UNSIGNED_BYTE, g->bitmap.buffer
        );

        ch.Advance = glm::vec2(g->advance.x >> 6, g->advance.y >> 6);
        ch.BitmapSize = glm::vec2(g->bitmap.width, g->bitmap.rows);
        ch.BitmapOffset = glm::vec2(g->bitmap_left, g->bitmap_top);
        ch.Offset = glm::vec2(static_cast<float>(xOffset) / TEXT_ATLAS_SIZE.x, 0);

        Characters[static_cast<int>(c)] = ch;
        xOffset += g->bitmap.width;
    }

    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    TextShader->Upload("text", TEXT_TEXTURE_UNIT);
    TextShader->Upload("projection", glm::ortho(
        0.0f, static_cast<float>(SCREEN_WIDTH),
        0.0f, static_cast<float>(SCREEN_HEIGHT)
    ));
}

void Interface::Mouse_Handler(int x, int y) {
    if (ActiveDocument == "") {
        return;
    }

    if (Holding && HoveringType == "slider") {
        static_cast<Slider*>(HoveringElement)->Move(static_cast<float>(x));
        return;
    }

    if (Holding && HoveringType == "textBox") {
        static_cast<TextBox*>(HoveringElement)->Drag();
        return;
    }

    if (HoveringType == "slot")    {
        static_cast<Slot*>(HoveringElement)->Stop_Hover();
    }
    else if (HoveringType == "button") {
        static_cast<Button*>(HoveringElement)->Color = BUTTON_COLOR;
    }
    else if (HoveringType == "slider") {
        static_cast<Slider*>(HoveringElement)->HandleColor = SLIDER_HANDLE_COLOR;
    }

    for (auto &element : Documents[ActiveDocument]) {
        if (element.second->Get_Hover(x, y, Holding)) {
            return;
        }
    }

    if (ActiveDocument == UI::CustomDocument) {
        for (auto &element : Documents["inventory"]) {
            if (element.second->Get_Hover(x, y, Holding)) {
                return;
            }
        }
    }

    HoveringType = "";
    HoveringElement = nullptr;

    System::Set_Cursor(0);
}

void Interface::Click(int mouseButton, int action) {
    if (HoveringType == "" && ActiveType == "") {
        Holding = false;
        return;
    }

    Holding = (action == GLFW_PRESS);

    if (Holding && ActiveType == "textBox" && HoveringElement != ActiveElement) {
        TextBox* textBox = static_cast<TextBox*>(ActiveElement);
        textBox->Unfocus();
    }

    if (HoveringType == "slot") {
        Slot* slot = static_cast<Slot*>(HoveringElement);

        if (Holding) {
            Inventory::Press_Slot(slot, mouseButton);
        }
        else {
            Inventory::Release_Slot();
        }
    }

    else if (HoveringType == "button") {
        Button* button = static_cast<Button*>(HoveringElement);

        if (Holding) {
            button->Color = BUTTON_CLICK_COLOR;
        }
        else {
            button->Release();
        }
    }

    else if (HoveringType == "slider") {
        Slider* slider = static_cast<Slider*>(HoveringElement);

        if (Holding) {
            slider->HandleColor = SLIDER_HANDLE_CLICK_COLOR;
        }
        else {
            slider->Release();
        }
    }

    else if (HoveringType == "textBox") {
        TextBox* textBox = static_cast<TextBox*>(HoveringElement);

        if (Holding) {
            if (textBox->Focused) {
                textBox->Click();
            }
            else {
                textBox->Focus();
            }
        }
    }

    Inventory::Mouse_Handler(UI::MouseX, UI::MouseY);
}

void Interface::Draw_Document(std::string document) {
    glDisable(GL_DEPTH_TEST);

    for (auto &element : Documents[document]) {
        element.second->Draw();
    }

    glEnable(GL_DEPTH_TEST);
}

float Interface::Get_String_Width(std::string string) {
    float currentWidth = 0;
    for (char const &c : string) { currentWidth += Characters[c].Advance.x; }
    return currentWidth;
}

std::vector<std::string> Interface::Get_Fitting_String(std::string string, int width) {
    float currentWidth = 0;
    unsigned long index = 0;
    size_t prevIndex = 0;
    bool addedString = false;
    bool ignoreNext = false;

    std::vector<std::string> partStrings;

    for (char const &c : string) {
        if (c == '&' || ignoreNext) {
            ignoreNext = !ignoreNext;
        }

        else {
            currentWidth += Characters[c].Advance.x;
            addedString = currentWidth > width;

            if (addedString) {
                if (c != ' ') {
                    size_t lastSpacePos = string.substr(prevIndex, index).rfind(' ');

                    if (lastSpacePos != std::string::npos) {
                        partStrings.push_back(string.substr(prevIndex, lastSpacePos));
                        prevIndex = lastSpacePos + 1;
                        currentWidth = Get_String_Width(string.substr(prevIndex, index - prevIndex));
                    }

                    else {
                        partStrings.push_back(string.substr(prevIndex, index - prevIndex));
                        prevIndex = index;
                        currentWidth = 0;
                    }
                }

                else {
                    partStrings.push_back(string.substr(prevIndex, index - prevIndex));
                    prevIndex = index + 1;
                    currentWidth = 0;
                }
            }
        }

        ++index;
    }

    if (!addedString) {
        partStrings.push_back(string.substr(prevIndex));
    }

    return partStrings;
}

namespace Interface {
    void Set_Document(std::string document) {
        ActiveDocument = document;
    }

    void Delete(std::string name) {
        Documents[ActiveDocument].erase(name);
    }
};
