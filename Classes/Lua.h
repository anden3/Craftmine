#pragma once

#include <map>
#include <string>

#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>

extern lua_State* State;

struct FunctionTable {
    luabridge::LuaRef Open;
    luabridge::LuaRef Update;
    luabridge::LuaRef Close;

    FunctionTable() : Open(State), Update(State), Close(State) {}

    FunctionTable(lua_State* L) : Open(L), Update(L), Close(L) {
        Close  = luabridge::getGlobal(L, "Close");
        Update = luabridge::getGlobal(L, "Update");
        Open   = luabridge::getGlobal(L, "Right_Click");
    }

    luabridge::LuaRef& operator[] (std::string name) {
        if      (name == "Open")   { return Open;   }
        else if (name == "Update") { return Update; }
        else if (name == "Close")  { return Close;  }
        return Open;
    }
};

namespace Lua {
    extern std::map<std::string, FunctionTable> Functions;

    void Init();
    void Load_Scripts();
    void Close();

    template<typename... Args>
    void Run(std::string block, std::string func, Args... args) {
        if (Functions.count(block)) {
            Functions[block][func](args...);
        }
    }

    void Register(std::string name);
};
