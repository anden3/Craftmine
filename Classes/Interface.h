#pragma once

#include <memory>
#include <string>

#include "Stack.h"
#include "Buffer.h"

#define EXPAND_VEC2(V) V.x, V.y
#define EXPAND_VEC3(V) V.x, V.y, V.z
#define EXPAND_VEC4(V) V.x, V.y, V.z, V.w

typedef void (Func)(void*);
typedef std::vector<float> Data;

struct Block;

inline std::string Format_Vector(glm::vec3 vector) {
    return std::string(
        std::to_string(int(vector.x)) + ", " + std::to_string(int(vector.y)) + ", " + std::to_string(int(vector.z))
    );
}

template <typename T>
inline bool In_Range(T value, glm::dvec2 bounds) {
    return value >= bounds.x && value <= (bounds.x + bounds.y);
}

template <typename A, typename B, typename C>
inline bool In_Range(A value, B min, C max) {
    return value >= min && value <= (min + max);
}

template <typename T>
inline Data Get_Rect(T x1, T x2, T y1, T y2) {
    return Data {x1, y1, x2, y1, x2, y2, x1, y1, x2, y2, x1, y2};
}

template <typename T>
inline Data Get_Border(T x1, T x2, T y1, T y2) {
    return Data {
        x1, y1, x2, y1, x2, y1, x2, y2, x2, y2,
        x1, y2, x1, y2, x1, y1, x1 - 0.5f, y2, x1, y2
    };
}

template <typename T>
inline Data Get_Tex_Rect(T x1, T x2, T y1, T y2) {
    return Data {
        x1, y1, 0, 1, x2, y1, 1, 1, x2, y2, 1, 0,
        x1, y1, 0, 1, x2, y2, 1, 0, x1, y2, 0, 0
    };
}

Data Get_3D_Mesh(const Block* block, float x, float y, bool offsets = false);

std::tuple<unsigned int, int, int> Load_Texture(std::string file, bool mipmap = false, float afLevel = 16.0f);
unsigned int Load_Array_Texture(std::string file, glm::ivec2 subCount, int mipmap = 0, float afLevel = 16.0f);

void Take_Screenshot();

template <typename V, typename T>
inline void Extend(std::vector<V> &storage, T t) { storage.push_back(V(t)); }

template <typename V, typename T, typename... Args>
inline void Extend(std::vector<V> &storage, T t, Args... args) {
    storage.push_back(V(t));
    Extend(storage, args...);
}

template <typename T>
inline void Extend(std::vector<T> &storage, std::vector<T> input) {
    for (T const &element : input) {
        storage.push_back(element);
    }
}

template <typename T>
inline void Extend(std::vector<T> &storage, glm::vec2 input) {
    Extend(storage, T(input.x), T(input.y));
}

template <typename T>
inline void Extend(std::vector<T> &storage, glm::vec3 input) {
    Extend(storage, T(input.x), T(input.y), T(input.z));
}

template <typename T>
inline void Extend(std::vector<T> &storage, glm::vec4 input) {
    Extend(storage, T(input.x), T(input.y), T(input.z), T(input.w));
}

class BaseElement {
public:
    virtual void Draw();
    virtual bool Get_Hover(int cursorX, int cursorY, bool holding);

    template<class E>
    E* Get_Type() { return static_cast<E*>(this); }
};

class Custom : public BaseElement {
public:
    std::string Name;

    float X;
    float Y;

    glm::vec3 Color;

    Buffer Storage;

    Custom() {}
    Custom(std::string name, float x, float y, Data &data);

private:
    glm::mat4 ModelMatrix;

    void Draw();
};

class TextElement : public BaseElement {
public:
    std::string Name;
    std::string Text = "";

    float X;
    float Y;
    float Scale;
    float Opacity;

    float Height;
    float Width;

    glm::vec3 Color;

    TextElement() {}
    TextElement(std::string name, std::string text, float x, float y) {
        Init(name, text, x, y);
    }
    TextElement(std::string name, std::string text, glm::vec2 size) {
        Init(name, text, size.x, size.y);
    }

    void Init(std::string name, std::string text, float x, float y, float opacity = 1.0f,
        glm::vec3 color = {1, 1, 1}, float scale = 1.0f);

    void Center(glm::vec2 pos, float width, glm::bvec2 axes = {true, true}) {
        Center(pos.x, pos.y, width, axes);
    }

    void Draw();
    void Mesh();
    void Set_Text(std::string newText);
    void Center(float x, float y, float width, glm::bvec2 axes = {true, true});

private:
    Buffer TextBuffer;

    float OriginalX = 0;
    float OriginalY = 0;
    float CenterWidth = 0;
    glm::bvec2 Centered = {false, false};

    float Get_Width();
};

class UIElement {
public:
    std::string Name;

    float X;
    float Y;
    float Opacity;

    float Width;
    float Height;

    glm::vec3 Color;

    TextElement Text;

    Buffer BackgroundBuffer;
    Buffer BorderBuffer;

    Func* Function;
};

class Button : public UIElement, public BaseElement  {
public:
    Button() {}
    Button(std::string name, std::string text, float x, float y, float w, float h, Func function) {
        Init(name, text, glm::vec4(x, y, w, h), function);
    }
    Button(std::string name, std::string text, glm::vec4 dims, Func function) {
        Init(name, text, dims, function);
    }

    void Release();

private:
    void Draw();
    bool Get_Hover(int cursorX, int cursorY, bool holding);
    void Init(std::string name, std::string text, glm::vec4 dims, Func function);
};

class Slider : public UIElement, public BaseElement {
public:
    float Value;
    float HandlePosition;
    glm::vec3 HandleColor;

    Slider() {}
    Slider(std::string name, std::string text, float x, float y, float w, float h, float min, float max, float value, Func function) {
        Init(name, text, glm::vec4(x, y, w, h), glm::vec3(min, max, value), function);
    }
    Slider(std::string name, std::string text, glm::vec4 dims, glm::vec3 range, Func function) {
        Init(name, text, dims, range, function);
    }

    void Release();
    void Move(float position, bool setValue = false);

private:
    float Min;
    float Max;
    float HandleOpacity;

    Buffer HandleBuffer;

    void Draw();
    bool Get_Hover(int cursorX, int cursorY, bool holding);
    void Init(std::string name, std::string text, glm::vec4 dims, glm::vec3 range, Func function);
};

class Bar : public UIElement, public BaseElement {
public:
    float Value;
    float BarOpacity;
    glm::vec3 BarColor;

    Bar() {}
    Bar(std::string name, std::string text, float x, float y, float w, float h, float min, float max, float value) {
        Init(name, text, glm::vec4(x, y, w, h), glm::vec3(min, max, value));
    }
    Bar(std::string name, std::string text, glm::vec4 dims, glm::vec3 range) {
        Init(name, text, dims, range);
    }

    void Move(float value);

private:
    float Min;
    float Max;

    Buffer BarBuffer;

    void Draw();
    void Init(std::string name, std::string text, glm::vec4 dims, glm::vec3 range);
};

class Image : public UIElement, public BaseElement {
public:
    Image() {}
    Image(std::string name, std::string file, int texID, float x, float y, float scale) {
        Init(name, file, texID, glm::vec3(x, y, scale));
    }
    Image(std::string name, std::string file, int texID, glm::vec3 dims) {
        Init(name, file, texID, dims);
    }

    void Center();

private:
    float Scale;

    unsigned int Texture;
    int TexID;

    Buffer ImageBuffer;

    void Draw();
    void Init(std::string name, std::string file, int texID, glm::vec3 dims);
};

class Background : public UIElement, public BaseElement {
public:
    glm::vec3 GridColor;

    Background() {}
    Background(std::string name, float x, float y, float w, float h, bool border = false, glm::vec2 gridWidth = {0, 0}, glm::vec2 pad = {0, 0}) {
        Init(name, glm::vec4(x, y, w, h), border, gridWidth, pad);
    }
    Background(std::string name, glm::vec4 dims, bool border = false, glm::vec2 gridWidth = {0, 0}, glm::vec2 pad = {0, 0}) {
        Init(name, dims, border, gridWidth, pad);
    }

    void Draw();

    void Move(float dx = 0, float dy = 0, bool absolute = false);
    void Move(glm::vec2 pos = glm::vec2(0, 0), bool absolute = false) {
        Move(pos.x, pos.y, absolute);
    }

    void Init(std::string name, glm::vec4 dims, bool border, glm::vec2 gridWidth, glm::vec2 pad);

private:
    glm::vec2 GridWidth;
    Buffer GridBuffer;

    bool GridSet = false;
};

class TextBox : public UIElement, public BaseElement {
public:
    bool Focused = false;
    bool Visible = true;

    TextElement Cursor;

    std::string Text = "";
    std::string MarkedText = "";

    TextBox() {}
    TextBox(std::string name, float x, float y, float w, float h) {
        Init(name, glm::vec4(x, y, w, h));
    }
    TextBox(std::string name, glm::vec4 dims) {
        Init(name, dims);
    }

    void Drag();
    void Clear();
    void Click();
    void Focus();
    void Unfocus();

    void Key_Handler(int key);
    void Add_Text(std::string text);
    void Input(unsigned int codepoint);
    void Set_Cursor_Visibility(bool cursorVisible);

private:
    Background BG;
    Background Highlight;

    TextElement TextEl;

    unsigned long CursorPos   = 0;
    unsigned long MarkerPos   = 0;
    unsigned long SelectEnd   = 0;
    unsigned long SelectStart = 0;

    float TextWidth = 0.0f;
    float MaxWidth = 0.0f;

    double LastBlink = 0.0;

    void Draw();
    void Paste();
    void Update();

    void Init(std::string name, glm::vec4 dims);
    void Select(unsigned long a, unsigned long b);

    unsigned long Get_Pos(int x);
    bool Get_Hover(int cursorX, int cursorY, bool holding);
};

class OrthoElement : public BaseElement {
public:
    int Type;

    float X;
    float Y;
    float Scale;

    std::string Name;

    OrthoElement() {}
    OrthoElement(std::string name, int type, int data, float x, float y, float scale);

    void Draw();
    void Mesh(int type, int data, float x = 0, float y = 0);
    void Mesh(int type, int data, glm::vec2 pos) { Mesh(type, data, pos.x, pos.y); }

private:
    glm::mat4 ModelMatrix;
    Buffer OrthoBuffer;
};

class Slot : public UIElement, public BaseElement {
public:
    bool OutputOnly = false;
    bool CraftingInput = false;
    bool CraftingOutput = false;

    Stack Contents = Stack();
    Slot* SyncedSlot = nullptr;

    Slot() {}
    Slot(std::string name, float x, float y, float scale, Stack contents = Stack()) {
        Init(name, glm::vec2(x, y), scale, contents);
    }
    Slot(std::string name, glm::vec2 pos, float scale, Stack contents = Stack()) {
        Init(name, pos, scale, contents);
    }

    void Mesh();
    void Hover();
    void Stop_Hover();
    void Swap_Stacks(Stack &stack);
    void Set_Contents(const Stack &stack);

private:
    bool Hovering = false;
    float SlotSize;

    Background BG;
    TextElement ItemCount;
    OrthoElement ItemModel;

    void Draw();
    bool Get_Hover(int cursorX, int cursorY, bool holding);
    void Init(std::string name, glm::vec2 size, float scale, Stack contents);
};

extern std::map<
    std::string, std::map<std::string, std::shared_ptr<BaseElement>>
> Documents;

#ifndef WIN32
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-parameter"
#endif

template<typename ...Args>
static std::string Get_Name(std::string name, Args&&... args) {
    return name;
}

#ifndef WIN32
#pragma clang diagnostic pop
#endif

namespace Interface {
    extern bool Holding;

    extern void* ActiveElement;
    extern void* HoveringElement;

    extern std::string ActiveType;
    extern std::string HoveringType;
    extern std::string ActiveDocument;

    void Init();
    void Init_Text();
    void Init_Shaders();
    void Init_UI_Scale();

    void Mouse_Handler(int x, int y);
    void Click(int mouseButton, int action);

    void Set_Document(std::string document);
    void Draw_Document(std::string document);

    float Get_String_Width(std::string string);
    std::vector<std::string> Get_Fitting_String(std::string string, int width);

    template<class E, typename ...Args>
    E* Add(Args... args) {
        const int nArgs = sizeof...(args);

        if (nArgs == 0) {
            return nullptr;
        }

        std::string name = Get_Name(args...);
        Documents[ActiveDocument][name] = std::shared_ptr<BaseElement>(new E(args...));

        return dynamic_cast<E*>(Documents[ActiveDocument][name].get());
    }

    template <class T>
    T* Get(std::string name) {
        return dynamic_cast<T*>(Documents[ActiveDocument][name].get());
    }

    void Delete(std::string name);
};
