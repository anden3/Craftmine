#pragma once

#include <glm/glm.hpp>

#include <lua.hpp>
#include <LuaBridge/LuaBridge.h>

class Slot;
class Custom;
class Background;
class TextElement;

struct Block;
struct Stack;

glm::vec2 Lua_Scale(int x, int y);

glm::vec3 Get_Player_Pos();
glm::vec3 Get_Player_Looking_Pos();

const Block* Lua_Get_Block_From_Stack(::Stack* stack);

void Set_Document(std::string doc);
void Set_Mouse_Active(bool state);
void Set_Custom_UI_Document(std::string doc);

TextElement* Add_Text(std::string name, std::string text, glm::vec2 pos);

Slot* Add_Slot(
    std::string name, glm::vec2 pos, float scale, luabridge::LuaRef contents
);

Background* Add_Background(
    std::string name, glm::vec2 pos, glm::vec2 size,
    bool border = false, glm::vec2 gridWidth = {0, 0}, glm::vec2 pad = {0, 0}
);

Custom* Add_Custom(std::string name, glm::vec2 pos, luabridge::LuaRef data);

Slot* Get_Slot(std::string name);
