#include "Lua.h"

#include <dirent.h>

#include "Blocks.h"
#include "Interface.h"
#include "Comparators.h"
#include "Lua_Interface.h"

lua_State* State = nullptr;

std::map<std::string, FunctionTable> Lua::Functions = {};

struct Vec2Helper : public glm::vec2 {
    glm::vec2 operator+ (glm::vec2 &other) {
        return glm::vec2(x + other.x, y + other.y);
    }
    glm::vec2 operator- (glm::vec2 &other) {
        return glm::vec2(x - other.x, y - other.y);
    }
    glm::vec2 operator* (float other) {
        return glm::vec2(x * other, y * other);
    }
    glm::vec2 operator/ (float other) {
        return glm::vec2(x / other, y / other);
    }
    bool operator== (glm::vec2 &other) {
        return x == other.x && y == other.y;
    }
};

struct Vec3Helper : public glm::vec3 {
    glm::vec3 operator+ (glm::vec3 &other) {
        return glm::vec3(x + other.x, y + other.y, z + other.z);
    }
    glm::vec3 operator- (glm::vec3 &other) {
        return glm::vec3(x - other.x, y - other.y, z - other.z);
    }
    glm::vec3 operator* (float other) {
        return glm::vec3(x * other, y * other, z * other);
    }
    glm::vec3 operator/ (float other) {
        return glm::vec3(x / other, y / other, z / other);
    }
    bool operator== (glm::vec3 &other) {
        return x == other.x && y == other.y && z == other.z;
    }
};

static void Get_Error() {
    const char* message = lua_tostring(State, -1);
    puts(message);
    lua_pop(State, 1);
}

static void Load(std::string file) {
    std::string block = file.substr(file.rfind('/') + 1, file.rfind('.') - file.rfind('/') - 1);
    int status = luaL_loadfile(State, file.c_str());

    if (status) {
        Get_Error();
        return;
    }

    lua_pcall(State, 0, 0, 0);
    lua_getglobal(State, "Init");
    status = lua_pcall(State, 0, 0, 0);

    if (status) {
        Get_Error();
    }

    Lua::Functions.emplace(block, FunctionTable(State));
    Blocks::Set_Interactable(block);
}

void Lua::Init() {
    State = luaL_newstate();
    luaL_openlibs(State);

    luabridge::getGlobalNamespace(State)
        .beginClass<Block>("Block")
            // bool
            .addData("IsTool",        &Block::IsTool)
            .addData("HasIcon",       &Block::HasIcon)
            .addData("IsMBRoot",      &Block::IsMBRoot)
            .addData("Collision",     &Block::Collision)
            .addData("Craftable",     &Block::Craftable)
            .addData("FullBlock",     &Block::FullBlock)
            .addData("Placeable",     &Block::Placeable)
            .addData("Smeltable",     &Block::Smeltable)
            .addData("Targetable",    &Block::Targetable)
            .addData("HasTexture",    &Block::HasTexture)
            .addData("MultiBlock",    &Block::MultiBlock)
            .addData("Transparent",   &Block::Transparent)
            .addData("Interactable",  &Block::Interactable)
            .addData("HasCustomData", &Block::HasCustomData)
            .addData("MultiTextures", &Block::MultiTextures)

            // int
            .addData("ID",                  &Block::ID)
            .addData("Data",                &Block::Data)
            .addData("Icon",                &Block::Icon)
            .addData("Texture",             &Block::Texture)
            .addData("Durability",          &Block::Durability)
            .addData("Luminosity",          &Block::Luminosity)
            .addData("MiningLevel",         &Block::MiningLevel)
            .addData("MiningSpeed",         &Block::MiningSpeed)
            .addData("CraftingYield",       &Block::CraftingYield)
            .addData("RequiredMiningLevel", &Block::RequiredMiningLevel)

            // float
            .addData("BurnTime", &Block::BurnTime)
            .addData("Hardness", &Block::Hardness)

            // std::string
            .addData("Name",              &Block::Name)
            .addData("Sound",             &Block::Sound)
            .addData("Material",          &Block::Material)
            .addData("EffectiveMaterial", &Block::EffectiveMaterial)

            // glm::vec3
            .addData("Scale",       &Block::Scale)
            .addData("MBOffset",    &Block::MBOffset)
            .addData("ScaleOffset", &Block::ScaleOffset)

            // Stack
            .addData("Drop",        &Block::Drop)
            .addData("SmeltResult", &Block::SmeltResult)
        .endClass()

        .beginClass<::Stack>("Stack")
            .addConstructor<void(*)(float, float, float)>()

            .addData("Type", &Stack::Type)
            .addData("Size", &Stack::Size)
            .addData("Data", &Stack::Data)

            .addFunction("Decrease", &Stack::Decrease)

            .addFunction("__eq",  &Stack::operator==)
        .endClass()

        .beginClass<glm::vec2>("vec2")
            .addConstructor<void(*)(float, float)>()

            .addData("x", &Vec2Helper::x)
            .addData("y", &Vec2Helper::y)

            .addFunction("__add", &Vec2Helper::operator+)
            .addFunction("__sub", &Vec2Helper::operator-)
            .addFunction("__mul", &Vec2Helper::operator*)
            .addFunction("__div", &Vec2Helper::operator/)
            .addFunction("__eq",  &Vec2Helper::operator==)
        .endClass()

        .beginClass<glm::vec3>("vec3")
            .addConstructor<void(*)(float, float, float)>()

            .addData("x", &Vec3Helper::x)
            .addData("y", &Vec3Helper::y)
            .addData("z", &Vec3Helper::z)

            .addFunction("__add", &Vec3Helper::operator+)
            .addFunction("__sub", &Vec3Helper::operator-)
            .addFunction("__mul", &Vec3Helper::operator*)
            .addFunction("__div", &Vec3Helper::operator/)
            .addFunction("__eq",  &Vec3Helper::operator==)
        .endClass()

        .addFunction("Get_Block", Lua_Get_Block_From_Stack)

        .beginNamespace("UI")
            .addFunction("Scale", Lua_Scale)
            .addFunction("Set_Mouse_Active", Set_Mouse_Active)
            .addFunction("Set_Custom_Document", Set_Custom_UI_Document)
        .endNamespace()

        .beginNamespace("Player")
            .addFunction("Get_Pos", Get_Player_Pos)
            .addFunction("Get_Looking_Pos", Get_Player_Looking_Pos)
        .endNamespace()

        .beginNamespace("Interface")
            .beginClass<Slot>("Slot")
                .addData("OutputOnly", &Slot::OutputOnly)
                .addData("Contents", &Slot::Contents)

                .addFunction("Set", &Slot::Set_Contents)
            .endClass()

            .beginClass<Custom>("Custom")
                .addData("Color", &Custom::Color)
            .endClass()

            .beginClass<Background>("Background").endClass()

            .addFunction("Set_Document", Set_Document)

            .addFunction("Add_Slot", Add_Slot)
            .addFunction("Add_Text", Add_Text)
            .addFunction("Add_Custom", Add_Custom)
            .addFunction("Add_Background", Add_Background)

            .addFunction("Get_Slot", Get_Slot)
        .endNamespace()
    ;
}

void Lua::Load_Scripts() {
    DIR* scriptDir = opendir("BlockScripts");
    struct dirent* scriptEnt;

    while ((scriptEnt = readdir(scriptDir)) != nullptr) {
        std::string fileName(scriptEnt->d_name);

        if (fileName.find(".lua") == std::string::npos) {
            continue;
        }

        Load("BlockScripts/" + fileName);
    }
}

void Lua::Close() {
    lua_close(State);
}
