#include "Lua_Interface.h"

#include "UI.h"
#include "main.h"
#include "Chunk.h"
#include "Stack.h"
#include "Blocks.h"
#include "Player.h"
#include "Interface.h"

using namespace luabridge;

static ::Stack Ref_To_Stack(LuaRef ref) {
    if (ref.isTable()) {
        if (ref.length() == 2) {
            if (ref[1].isNumber()) {
                return {ref[1].cast<int>(), ref[2]};
            }
            else {
                return {ref[1].cast<std::string>(), ref[2]};
            }
        }
        else {
            return {ref[1], ref[2], ref[3]};
        }
    }
    else if (ref.isString()) {
        return {ref.cast<std::string>(), 1};
    }

    return {ref.cast<int>()};
}

const Block* Lua_Get_Block_From_Stack(::Stack* stack) {
    return Blocks::Get_Block(*stack);
}

glm::vec2 Lua_Scale(int x, int y) {
    return Scale(x, y);
}

glm::vec3 Get_Player_Pos() {
    return Get_World_Pos(player.CurrentChunk, player.CurrentTile);
}

glm::vec3 Get_Player_Looking_Pos() {
    return Get_World_Pos(player.LookingChunk, player.LookingTile);
}

void Set_Document(std::string doc) {
    Interface::Set_Document(doc);
}

void Set_Custom_UI_Document(std::string doc) {
    UI::CustomDocument = doc;
}

void Set_Mouse_Active(bool state) {
    UI::Set_Mouse_Active(state);
}

TextElement* Add_Text(std::string name, std::string text, glm::vec2 pos) {
    return Interface::Add<TextElement>(name, text, pos);
}

Background* Add_Background(std::string name, glm::vec2 pos, glm::vec2 size, bool border, glm::vec2 gridWidth, glm::vec2 pad) {
    return Interface::Add<Background>(
        name, glm::vec4(pos, size), border, gridWidth, pad
    );
}

Slot* Add_Slot(std::string name, glm::vec2 pos, float scale, LuaRef contents) {
    ::Stack s;

    if (!contents.isNil()) {
        s = Ref_To_Stack(contents);
    }

    return Interface::Add<Slot>(name, pos, scale, s);
}

Custom* Add_Custom(std::string name, glm::vec2 pos, LuaRef data) {
    Data dataList;

    for (int i = 1; i <= data.length(); ++i) {
        dataList.push_back(data[i]);
    }

    return Interface::Add<Custom>(name, pos.x, pos.y, dataList);
}

Slot* Get_Slot(std::string name) {
    return Interface::Get<Slot>(name);
}
